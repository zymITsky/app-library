package com.qkun.library.base;

import android.app.Application;
import android.content.Context;

import com.qkun.library.utils.ActivityManager;
import com.qkun.library.utils.Utils;

public class BaseApplication extends Application {

    private static Context sAppContext;

    public static synchronized Context getAppContext() {
        return Utils.requireNonNull(sAppContext);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sAppContext = getApplicationContext();
        registerActivityLifecycleCallbacks(ActivityManager.getInstance());
    }
}
