package com.qkun.library.base;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.qkun.library.utils.Utils;

/**
 * @author qinkun
 * @date 1/31 0031 13:22
 * @description
 */
public class TagActivity extends BaseActivity {
    public static final String DATA_TAG = "tag";
    private String mTag;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (Utils.nonNull(intent)) {
            mTag = intent.getStringExtra(DATA_TAG);
        }
    }

    public String getTag() {
        return mTag;
    }

    public boolean equalsTag(String tag) {
        if (mTag == null) {
            return tag == null;
        }
        return mTag.equals(tag);
    }
}
