package com.qkun.library.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.annotation.NonNull;

import com.qkun.library.utils.Utils;

public abstract class BaseBroadcastReceiver extends BroadcastReceiver {

    @Override
    public final void onReceive(Context context, Intent intent) {
        if (Utils.nonNull(intent)) {
            final String action = intent.getAction();
            if (Utils.nonNull(action)) {
                onReceive(context, intent, action);
            }
        }
    }

    protected abstract void onReceive(Context context, @NonNull Intent intent, @NonNull String action);

    public final void register(@NonNull Context context, @NonNull IntentFilter intentFilter) {
        context.registerReceiver(this, intentFilter);
    }

    public final void unregister(@NonNull Context context) {
        context.unregisterReceiver(this);
    }

}