package com.qkun.library.base;

import android.content.Intent;

/**
 * @author qinkun
 * @date 9/26 0026 10:04
 * @description
 */
public interface IActivityFragmentCompat {
    void startActivityForResult(Intent intent, int requestCode);
}
