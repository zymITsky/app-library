package com.qkun.library.base.binding.service;

public interface BindingProtocol {

    String DATA_PACKAGE_NAME = "data_package_name";
    String DATA_CALLING_UID = "data_calling_uid";
    String DATA_CALLING_PID = "data_calling_pid";
    String DATA_TOKEN = "data_token";
    String DATA_RESULT_RECEIVER = "data_result_receiver";

    int SERVICE_VERSION_1 = 1;
    int SERVICE_VERSION_CURRENT = SERVICE_VERSION_1;
    int CLIENT_VERSION_1 = 1;
    int CLIENT_VERSION_CURRENT = CLIENT_VERSION_1;

    int SERVICE_MSG_ON_CONNECT = Integer.MAX_VALUE;
    int SERVICE_MSG_ON_CONNECT_FAILED = Integer.MIN_VALUE;
    int CLIENT_MSG_CONNECT = Integer.MAX_VALUE;
    int CLIENT_MSG_DISCONNECT = Integer.MIN_VALUE;

}
