package com.qkun.library.base;

/**
 * <p>This Presenter is usually extended by the your Presenter
 * <code>
 * public interface Contract {
 * interface Presenter extends BasePresenter {
 * //to definition your method for presenter
 * }
 * interface View extends BaseView<Presenter> {
 * //to definition your method for view
 * }
 * }
 * </code>
 * </p>
 */
public interface BasePresenter {

    void start();

}
