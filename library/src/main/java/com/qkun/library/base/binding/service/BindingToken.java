package com.qkun.library.base.binding.service;

import android.os.Parcel;
import android.os.Parcelable;

final class BindingToken implements Parcelable {

    public static final Parcelable.Creator<BindingToken> CREATOR = new Parcelable.Creator<BindingToken>() {
        @Override
        public BindingToken createFromParcel(Parcel in) {
            return new BindingToken(in.readInt());
        }

        @Override
        public BindingToken[] newArray(int size) {
            return new BindingToken[size];
        }
    };
    private final int mKey;

    BindingToken(Object obj) {
        this(System.identityHashCode(obj));
    }

    BindingToken(int key) {
        mKey = key;
    }

    public static boolean isBindingToken(Object token) {
        return token instanceof BindingToken;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof BindingToken) {
            return mKey == ((BindingToken) obj).mKey;
        }
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mKey);
    }

}