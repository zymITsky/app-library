package com.qkun.library.base.receiver;

import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

public final class ScreenStateReceiver extends AnnotationBroadcastReceiver {

    private final List<Callback> mCallbacks = new ArrayList<>();

    /**
     * 点亮屏幕时的监听
     */
    @Action(Intent.ACTION_SCREEN_ON)
    private void notifyScreenOn() {
        final int N = mCallbacks.size();
        for (int i = 0; i < N; i++) {
            mCallbacks.get(i).onScreenOn();
        }
    }

    /**
     * 关闭屏幕时的监听
     */
    @Action(Intent.ACTION_SCREEN_OFF)
    private void notifyScreenOff() {
        final int N = mCallbacks.size();
        for (int i = 0; i < N; i++) {
            mCallbacks.get(i).onScreenOff();
        }
    }

    /**
     * 解锁时的监听
     */
    @Action(Intent.ACTION_USER_PRESENT)
    private void notifyUserPresent() {
        final int N = mCallbacks.size();
        for (int i = 0; i < N; i++) {
            mCallbacks.get(i).onUserPresent();
        }
    }

    public void addCallback(Callback callback) {
        mCallbacks.add(callback);
    }

    public void removeCallback(Callback callback) {
        mCallbacks.remove(callback);
    }

    public void clearCallback() {
        mCallbacks.clear();
    }

    public interface Callback {
        void onScreenOn();

        void onScreenOff();

        void onUserPresent();
    }
}