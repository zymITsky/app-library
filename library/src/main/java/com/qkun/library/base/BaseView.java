package com.qkun.library.base;

import androidx.annotation.NonNull;

/**
 * <p>This view is usually extended by the activity
 * <code>
 * public interface Contract {
 * interface Presenter extends BasePresenter {
 * //to definition your method for presenter
 * }
 * interface View extends BaseView<Presenter> {
 * //to definition your method for view
 * }
 * }
 * </code>
 * </p>
 */
public interface BaseView<T> {

    void setPresenter(@NonNull T presenter);

    void init();

}
