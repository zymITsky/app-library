package com.qkun.library.base;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.qkun.library.utils.ResultProcessor;

public class BaseActivity extends AppCompatActivity implements IActivityFragmentCompat {

    /**
     * Standard activity result: Start fail.
     */
    public static final int RESULT_FAIL = Integer.MIN_VALUE;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ResultProcessor.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ResultProcessor.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
