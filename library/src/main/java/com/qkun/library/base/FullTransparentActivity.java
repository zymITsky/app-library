package com.qkun.library.base;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.qkun.library.R;

/**
 * @author qinkun
 * @date 1/31 0031 13:22
 * @description
 */
public class FullTransparentActivity extends TagActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.TransparentStyle);
    }
}
