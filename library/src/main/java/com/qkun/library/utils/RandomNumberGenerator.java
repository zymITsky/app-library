package com.qkun.library.utils;

import java.util.Random;

/**
 * @author qinkun
 * @date 12/10 0010 10:23
 * @description
 */
public final class RandomNumberGenerator {
    private final Random mRandom = new Random();

    public static RandomNumberGenerator getInstance() {
        return Holder.instance;
    }

    public int nextInt(int bound) {
        return mRandom.nextInt(bound);
    }

    public int nextInt(int start, int end) {
        return nextInt(end - start) + start;
    }

    public int[] randomSortInts(int[] src) {
        if (!ArraysEx.isEmpty(src)) {
            for (int i = 0; i < src.length; i++) {
                int index = nextInt(i, src.length);
                int temp = src[i];
                src[i] = src[index];
                src[index] = temp;
            }
        }
        return src;
    }

    private static class Holder {
        private static final RandomNumberGenerator instance = new RandomNumberGenerator();
    }

}
