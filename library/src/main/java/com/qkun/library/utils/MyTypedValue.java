package com.qkun.library.utils;

import android.util.DisplayMetrics;
import android.util.TypedValue;

import com.qkun.library.base.BaseApplication;

public class MyTypedValue extends TypedValue {

    public static final int COMPLEX_UNIT_PX_TO_DIP = 6;
    public static final int COMPLEX_UNIT_PX_TO_SP = 7;
    public static final int COMPLEX_UNIT_PX_TO_PT = 8;
    public static final int COMPLEX_UNIT_PX_TO_IN = 9;
    public static final int COMPLEX_UNIT_PX_TO_MM = 10;

    public static final int COMPLEX_UNIT_SP_TO_DIP = 11;
    public static final int COMPLEX_UNIT_PT_TO_DIP = 12;
    public static final int COMPLEX_UNIT_IN_TO_DIP = 13;
    public static final int COMPLEX_UNIT_MM_TO_DIP = 14;

    public static final int UNIT_MS_TO_S = 15;
    public static final int UNIT_MS_TO_M = 16;
    public static final int UNIT_MS_TO_H = 17;
    public static final int UNIT_MS_TO_D = 18;
    public static final int UNIT_MS_TO_M_S = 19;
    public static final int UNIT_MS_TO_H_M_S = 20;
    public static final int UNIT_MS_TO_D_H_M_S = 21;

    private static final long S = 1000L;
    private static final long M = 60000L;
    private static final long H = 3600000L;
    private static final long D = 86400000L;

    public static float apply(int unit, float value) {
        DisplayMetrics metrics = BaseApplication.getAppContext().getResources().getDisplayMetrics();
        switch (unit) {
            //以对应单位为标准
            case COMPLEX_UNIT_PX_TO_DIP:
                return value / metrics.density;
            case COMPLEX_UNIT_PX_TO_SP:
                return value / metrics.scaledDensity;
            case COMPLEX_UNIT_PX_TO_PT:
                return value / metrics.xdpi / (1.0f / 72);
            case COMPLEX_UNIT_PX_TO_IN:
                return value / metrics.xdpi;
            case COMPLEX_UNIT_PX_TO_MM:
                return value / metrics.xdpi / (1.0f / 25.4f);

            //以dp单位为标准
            case COMPLEX_UNIT_SP_TO_DIP:
                return apply(COMPLEX_UNIT_PX_TO_DIP, applyDimension(COMPLEX_UNIT_SP, value, metrics));
            case COMPLEX_UNIT_PT_TO_DIP:
                return apply(COMPLEX_UNIT_PX_TO_DIP, applyDimension(COMPLEX_UNIT_PT, value, metrics));
            case COMPLEX_UNIT_IN_TO_DIP:
                return apply(COMPLEX_UNIT_PX_TO_DIP, applyDimension(COMPLEX_UNIT_IN, value, metrics));
            case COMPLEX_UNIT_MM_TO_DIP:
                return apply(COMPLEX_UNIT_PX_TO_DIP, applyDimension(COMPLEX_UNIT_MM, value, metrics));
        }
        //以px单位为标准，转化为相应的单位
        return applyDimension(unit, value, metrics);
    }

    public static String applyTime(int unit, long value) {
        String time = "";
        switch (unit) {
            case UNIT_MS_TO_S:
                time += value / S;
                break;
            case UNIT_MS_TO_M:
                time += value / M;
                break;
            case UNIT_MS_TO_H:
                time += value / H;
                break;
            case UNIT_MS_TO_D:
                time += value / D;
                break;
            case UNIT_MS_TO_M_S:
                String m = applyTime(UNIT_MS_TO_M, value);
                String s = applyTime(UNIT_MS_TO_S, value % M);
                time = (m.length() < 2 ? "0" + m : m) + ":" + (s.length() < 2 ? "0" + s : s);
                break;
            case UNIT_MS_TO_H_M_S:
                String h = applyTime(UNIT_MS_TO_H, value);
                time = (h.length() < 2 ? "0" + h : h) + ":" + applyTime(UNIT_MS_TO_M_S, value % H);
                break;
            case UNIT_MS_TO_D_H_M_S:
                time = applyTime(UNIT_MS_TO_D, value) + "天" + applyTime(UNIT_MS_TO_H_M_S, value % D);
                break;
        }
        return time;
    }

}
