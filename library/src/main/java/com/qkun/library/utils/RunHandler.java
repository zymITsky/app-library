package com.qkun.library.utils;

import android.os.Handler;

import androidx.annotation.NonNull;

/**
 * @author qinkun
 * @date 10/25 0025 19:10
 * @description
 */
public class RunHandler extends Handler {
    public final void postOrRun(@NonNull Runnable r) {
        if (getLooper().getThread() != Thread.currentThread()) {
            post(r);
        } else {
            r.run();
        }
    }
}
