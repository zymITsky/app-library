package com.qkun.library.utils;

import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.widget.TextView;

import androidx.annotation.ColorRes;
import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;

import com.qkun.library.R;

/**
 * @author qinkun
 * @date 9/10 0010 23:35
 * @description
 */
public class TextUtilsEx {

    /**
     * 换行符
     */
    public static final String ENTER = System.getProperty("line.separator");

    /**
     * 判断textView的内容是否被省略
     * <p>
     * 请注意在textView测算结束之后调用，否在<code>TextView#getLayout()</code>会为null,
     * 并且该方法比较依赖于布局，另外一种方法是计算文本宽度，然后和textView可容纳空间做比对
     * </p>
     *
     * @param textView
     * @return
     */
    public static boolean hasEllipsis(TextView textView) {
        if (Utils.nonNull(textView)) {
            Layout layout = textView.getLayout();
            int lineCount = textView.getLineCount();
            if ((lineCount >= textView.getMaxLines()) && (layout != null)) {
                int ellipsisCount = layout.getEllipsisCount(lineCount - 1);
                return ellipsisCount != 0;
            }
        }
        return false;
    }

    /**
     * 根据文本宽度来判断文本是否会被省略
     * <p>
     * 该方法只适用于单行的情况，如果是多行的则很大可能会出错，因为字符串显示并不是排满一行才换行的
     * </p>
     *
     * @param textView
     * @return
     */
    public static boolean hasEllipsisForMeasure(TextView textView) {
        if (Utils.nonNull(textView)) {
            float textWidth = measureTextWidth(textView);
            float singleContentWidth = textView.getWidth() - textView.getCompoundPaddingLeft() - textView.getCompoundPaddingRight();
            return textWidth > singleContentWidth;
        }
        return false;
    }

    /**
     * 测算将在textView中显示的全部文本的宽度
     *
     * @param textView
     * @param text
     * @return
     */
    public static float measureTextWidth(TextView textView, String text) {
        if (Utils.nonNull(textView) && !TextUtils.isEmpty(text)) {
            return textView.getPaint().measureText(text);
        }
        return 0;
    }

    public static float measureTextWidth(TextView textView) {
        return measureTextWidth(textView, textView.getText().toString());
    }

    public static SpannableStringBuilder makeSpannableStringBuilder(CharSequence text, int start, int end) {
        return new SpannableStringBuilder(text, start, end);
    }

    public static SpannableStringBuilder makeSpannableStringBuilder(CharSequence text) {
        return new SpannableStringBuilder(text);
    }

    public static SpannableStringBuilder makeSpannableStringBuilder() {
        return new SpannableStringBuilder();
    }

    public static SpannableStringEx makeSpannableString(CharSequence source) {
        return new SpannableStringEx(source);
    }

    @NonNull
    public static SpannableStringEx buildSummaryText(CharSequence title, CharSequence content) {
        if (Utils.isNull(title)) title = "";
        if (Utils.isNull(content)) content = "";
        CharSequence sourceText = title.length() == 0 ? content : title + ENTER + content;
        int firstTextLength = title.length();
        int sourceTextLength = sourceText.length();
        return makeSpannableString(sourceText)
                .addAbsoluteSizeRes(R.dimen.buttonTextSize, 0, firstTextLength, SpannableStringEx.FLAG_IN_EX)
                .addAbsoluteSizeRes(R.dimen.subTextSize, firstTextLength, sourceTextLength, SpannableStringEx.FLAG_IN_EX)
                .addForegroundColorRes(R.color.colorTitle, 0, firstTextLength, SpannableStringEx.FLAG_IN_EX)
                .addForegroundColorRes(R.color.colorSubTitle, firstTextLength, sourceTextLength, SpannableStringEx.FLAG_IN_EX);
    }

    @NonNull
    public static SpannableStringEx buildText(CharSequence text, @DimenRes int sizeRes, @ColorRes int colorId) {
        if (Utils.isNull(text)) text = "";
        int length = text.length();
        return TextUtilsEx.makeSpannableString(text)
                .addAbsoluteSizeRes(sizeRes, 0, length, SpannableStringEx.FLAG_IN_EX)
                .addForegroundColorRes(colorId, 0, length, SpannableStringEx.FLAG_IN_EX);
    }

}
