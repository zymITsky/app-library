package com.qkun.library.utils;

import android.content.Intent;

/**
 * @author qinkun
 * @date 10/31 0031 13:04
 * @description
 */
public final class IntentUtils {

    public static Intent getSelectFileIntent(String type) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(type);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        return intent;
    }

    public static Intent getSelectFileIntent() {
        return getSelectFileIntent("*/*");//设置类型，我这里是任意类型，任意后缀的可以这样写。
    }
}
