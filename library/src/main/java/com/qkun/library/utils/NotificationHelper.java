package com.qkun.library.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.annotation.StringRes;
import androidx.core.app.NotificationCompat;

import com.qkun.library.base.BaseApplication;

/**
 * @author qinkun
 * @date 9/30 0030 22:47
 * @description
 */
public final class NotificationHelper {

    @NonNull
    public static NotificationManager getNotificationManager() {
        return (NotificationManager) BaseApplication.getAppContext().getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void createChannel(NotificationChannel channel) {
        if (Utils.nonNull(channel)) {
            NotificationManager manager = getNotificationManager();
            if (Utils.nonNull(manager)) {
                manager.createNotificationChannel(channel);
            }
        }
    }

    /**
     * @param channelId
     * @param channelName
     * @param importance  IMPORTANCE_NONE 关闭通知
     *                    IMPORTANCE_MIN 开启通知，不会弹出，不发出提示音，状态栏中无显示
     *                    IMPORTANCE_LOW 开启通知，不会弹出，不发出提示音，状态栏中显示
     *                    IMPORTANCE_DEFAULT 开启通知，不会弹出，发出提示音，状态栏中显示
     *                    IMPORTANCE_HIGH 开启通知，会弹出，发出提示音，状态栏中显示
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @NonNull
    public static NotificationChannel makeChannel(String channelId, String channelName, int importance) {
        return new NotificationChannel(channelId, channelName, importance);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @NonNull
    public static NotificationChannel getChannel(String channelId) {
        NotificationManager manager = getNotificationManager();
        if (Utils.nonNull(manager)) {
            return manager.getNotificationChannel(channelId);
        }
        return null;
    }

    public static boolean isChannelInactive(String channelId) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && Utils.isNull(getChannel(channelId));
    }

    @NonNull
    public static NotificationCompat.Builder getBuilder(String channelId,
                                                        @DrawableRes int smallIconResId,
                                                        @StringRes int titleResId,
                                                        @StringRes int textResId) {
        return getBuilder(channelId,
                smallIconResId,
                BaseApplication.getAppContext().getString(titleResId),
                BaseApplication.getAppContext().getString(textResId));
    }

    @NonNull
    public static NotificationCompat.Builder getBuilder(String channelId,
                                                        @DrawableRes int smallIconResId,
                                                        String title,
                                                        String text) {
        //设置此通知是否仅与当前设备相关，如果设置为true，通知就不能桥接到其他设备上进行远程显示
        //builder.setLocalOnly(true);
        return new NotificationCompat.Builder(BaseApplication.getAppContext(), channelId)
                .setSmallIcon(smallIconResId)
                .setContentTitle(title)
                .setContentText(text);
    }

    @NonNull
    public static Notification buildForegroundNotificationBase(@DrawableRes int iconResId,
                                                               @StringRes int titleResId,
                                                               @StringRes int textResId,
                                                               String channelId,
                                                               String channelName,
                                                               PendingIntent pendingIntent) {
        return buildForegroundNotificationBase(iconResId,
                BaseApplication.getAppContext().getString(titleResId),
                BaseApplication.getAppContext().getString(textResId),
                channelId,
                channelName,
                pendingIntent);
    }

    @NonNull
    public static Notification buildForegroundNotificationBase(@DrawableRes int iconResId,
                                                               String title,
                                                               String text,
                                                               String channelId,
                                                               String channelName,
                                                               PendingIntent pendingIntent) {
        if (isChannelInactive(channelId)) {
            createChannel(makeChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH));
            if (isChannelInactive(channelId)) return null;
        }
        return getBuilder(channelId, iconResId, title, text)
                .setAutoCancel(false)
                .setContentIntent(pendingIntent)
                .setShowWhen(false)
                .setLocalOnly(true)//设置此通知是否仅与当前设备相关，如果设置为true，通知就不能桥接到其他设备上进行远程显示
                .build();
    }
}
