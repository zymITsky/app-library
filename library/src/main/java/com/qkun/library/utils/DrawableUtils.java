package com.qkun.library.utils;

import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.util.Log;

import androidx.core.graphics.drawable.DrawableCompat;

import com.qkun.library.base.BaseApplication;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class DrawableUtils {

    /**
     * drawable转bitmap
     *
     * @param drawable
     * @return
     */
    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (Utils.nonNull(drawable)) {
            if (drawable instanceof BitmapDrawable) {
                BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
                Bitmap bitmap = bitmapDrawable.getBitmap();
                if (Utils.nonNull(bitmap)) {
                    return bitmap;
                }
            }
            int width = drawable.getIntrinsicWidth();
            int height = drawable.getIntrinsicHeight();
            if (width <= 0) width = 1;
            if (height <= 0) height = 1;
            Bitmap.Config config = drawable.getOpacity() == PixelFormat.OPAQUE ? Bitmap.Config.RGB_565 : Bitmap.Config.ARGB_8888;
            Bitmap bitmap = Bitmap.createBitmap(width, height, config);
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        }
        return null;
    }

    public static Drawable getDrawableOfSelectableItemBackground(boolean hasBorderless) {
        int[] attrs = new int[]{hasBorderless ? android.R.attr.selectableItemBackgroundBorderless : android.R.attr.selectableItemBackground};
        TypedArray ta = BaseApplication.getAppContext().obtainStyledAttributes(attrs);
        try {
            return ta.getDrawable(0);
        } finally {
            ta.recycle();
        }
    }

    /**
     * 生成位图的Base64格式字符串.
     *
     * @return 字符串.
     * 如果参数[bitmap]为空，返回空字符串.
     * @param[bitmap] 图片.
     */
    public static String bitmapToBase64(Bitmap bitmap) {
        String result = "";
        if (Utils.nonNull(bitmap)) {
            ByteArrayOutputStream byteArrayOutputStream = null;
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byteArrayOutputStream.flush();
                result = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
            } catch (IOException e) {
                MyLog.e(Log.getStackTraceString(e));
            } finally {
                try {
                    if (Utils.nonNull(byteArrayOutputStream)) {
                        byteArrayOutputStream.close();
                    }
                } catch (IOException e) {
                    MyLog.e(Log.getStackTraceString(e));
                }
            }
        }
        return result;
    }

    /**
     * 将保存为base64格式的位图转换回[Bitmap]类型的对象.
     *
     * @param base64Data input.
     * @return 位图对象.
     */
    public static Bitmap base64ToBitmap(String base64Data) {
        byte[] bytes = Base64.decode(base64Data, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public static Drawable toTintDrawable(Drawable drawable, int color) {
        if (Utils.nonNull(drawable)) {
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, color);
        }
        return drawable;
    }

    // TODO: 6/1 0001 待查看和 ContextCompat.getDrawable() 的区别
//    public static Drawable getDrawable(int resId) {
//        return getDrawable(resId,null);
//    }
//
//    public static Drawable getDrawable(int resId, Resources.Theme theme) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            return BaseApplication.getAppContext().getResources().getDrawable(resId, theme);
//        } else {
//            Bitmap bitmap = BitmapFactory.decodeResource(BaseApplication.getAppContext().getResources(), resId);
//            BitmapDrawable res = new BitmapDrawable(BaseApplication.getAppContext().getResources(), bitmap);
//            {if (Utils.nonNull(theme)) {
//                DrawableCompat.applyTheme(res, theme);
//            }
//            return res;
//        }
//    }

}
