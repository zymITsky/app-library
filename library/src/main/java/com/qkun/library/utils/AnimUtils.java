package com.qkun.library.utils;

import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.view.animation.Animation;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;

/**
 * @author qinkun
 * @date 10/31 0031 13:06
 * @description
 */
public final class AnimUtils {

    public static ObjectAnimator getScaleElasticityObjectAnimator(Object object) {
        Keyframe key1 = Keyframe.ofFloat(0, 1);
        Keyframe key2 = Keyframe.ofFloat(0.275f, 0.85f);
        Keyframe key3 = Keyframe.ofFloat(0.69f, 1.1f);
        Keyframe key4 = Keyframe.ofFloat(0.94f, 0.98f);
        Keyframe key5 = Keyframe.ofFloat(1, 1);
        PropertyValuesHolder holder = PropertyValuesHolder.ofKeyframe("scaleX", key1, key2, key3, key4, key5);
        ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(object, holder);
        animator.setDuration(544);
        return animator;
    }

    /**
     * 抖动
     */
    public static Animation getSharkAnimation(int counts) {
        Animation translateAnimation = new TranslateAnimation(0, 10, 0, 0);
        translateAnimation.setInterpolator(new CycleInterpolator(counts));
        translateAnimation.setDuration(1000);
        return translateAnimation;
    }

    public static Animation getDefaultSharkAnimation() {
        return getSharkAnimation(5);
    }
}
