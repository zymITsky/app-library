package com.qkun.library.utils;

import java.util.Collection;

/**
 * @author qinkun
 * @date 11/21 0021 11:20
 * @description
 */
public final class CollectionsEx {

    public static boolean isEmpty(Collection<?> collection) {
        return Utils.isNull(collection) || collection.size() == 0;
    }

    public static boolean isIncludeIndex(int collectionSize, int index) {
        return (index > -1) && (index < collectionSize);
    }

    public static boolean isIncludeIndex(Collection<?> collection, int index) {
        return Utils.nonNull(collection) && isIncludeIndex(collection.size(), index);
    }
}
