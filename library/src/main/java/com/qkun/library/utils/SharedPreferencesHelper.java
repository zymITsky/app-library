package com.qkun.library.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.qkun.library.base.BaseApplication;

/**
 * @author qinkun
 * @date 6/3 0003 23:25
 * @description SharedPreferences 保存位置都是 data/data/<package name>/shared_prefs/ 目录下，
 * 值得注意的是：<p>System.exit(0);//使用apply之后没多久就exit会导致写入失败，
 * 如果单单只是finish则是可以成功的，而使用commit则不受exit影响，但可能会影响性能</p>
 */
public final class SharedPreferencesHelper {

    private SharedPreferencesHelper() {
    }

    /**
     * 以应用程序保姆前缀作为文件名
     *
     * @return
     */
    public static SharedPreferences get() {
        return PreferenceManager.getDefaultSharedPreferences(BaseApplication.getAppContext());
    }

    public static SharedPreferences get(String name) {
        return BaseApplication.getAppContext().getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    /**
     * 以该 activity 对应类名作为文件名
     *
     * @param activity
     * @return
     */
    public static SharedPreferences get(Activity activity) {
        return Utils.requireNonNull(activity).getPreferences(Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor editor() {
        return get().edit();
    }

    public static SharedPreferences.Editor editor(String name) {
        return get(name).edit();
    }

    public static SharedPreferences.Editor editor(Activity activity) {
        return get(activity).edit();
    }

}
