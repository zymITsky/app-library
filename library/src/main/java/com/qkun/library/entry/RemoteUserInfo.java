package com.qkun.library.entry;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.qkun.library.utils.Utils;

public final class RemoteUserInfo {

    private final String mPackageName;
    private final int mPid;
    private final int mUid;

    public RemoteUserInfo(@NonNull String packageName, int pid, int uid) {
        mPackageName = packageName;
        mPid = pid;
        mUid = uid;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof RemoteUserInfo)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        RemoteUserInfo otherUserInfo = (RemoteUserInfo) obj;
        return TextUtils.equals(mPackageName, otherUserInfo.mPackageName)
                && (mPid == otherUserInfo.mPid)
                && (mUid == otherUserInfo.mUid);
    }

    @Override
    public int hashCode() {
        return Utils.hash(mPackageName, mPid, mUid);
    }

    public String getPackageName() {
        return mPackageName;
    }

    public int getPid() {
        return mPid;
    }

    public int getUid() {
        return mUid;
    }
}