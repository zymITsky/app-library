package com.qkun.library.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.content.ContextCompat;

import com.qkun.library.R;
import com.qkun.library.utils.Utils;

public class CleanableEditText extends AppCompatEditText {

    private Drawable mClearDrawable;
    private boolean mIsCleanable;

    public CleanableEditText(Context context) {
        this(context, null);
    }

    public CleanableEditText(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.editTextStyle);
    }

    public CleanableEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.CleanableEditText, defStyleAttr, R.style.ThemeOverlay_AppCompat);
        Drawable drawable = attributes.getDrawable(R.styleable.CleanableEditText_clearDrawable);
        attributes.recycle();
        if (Utils.isNull(drawable)) {
            drawable = ContextCompat.getDrawable(getContext(), R.mipmap.ic_clear);
        }
        setClearDrawable(drawable);
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        setClearIconVisible(hasFocus() && text.length() > 0);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        setClearIconVisible(focused && length() > 0);
    }

    private void setClearIconVisible(boolean visible) {
        if (mIsCleanable != visible) {
            mIsCleanable = visible;
            Drawable[] drawables = getCompoundDrawables();
            setCompoundDrawablesWithIntrinsicBounds(drawables[0], drawables[1],
                    visible ? mClearDrawable : null, drawables[3]);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (Utils.nonNull(getCompoundDrawables()[2])) {
                final float x = event.getX();
                final int width = getWidth();
                if (x >= (width - getTotalPaddingRight()) && x <= (width - getPaddingRight())) {
                    setText("");
                    event.setAction(MotionEvent.ACTION_CANCEL);
                }
            }
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void setCompoundDrawables(Drawable left, Drawable top, Drawable right, Drawable bottom) {
        if (Utils.nonNull(right)) {
            mClearDrawable = right;
            // 为 Drawable 设置为自身大小的宽高
            mClearDrawable.setBounds(0, 0,
                    mClearDrawable.getIntrinsicWidth(), mClearDrawable.getIntrinsicHeight());
        }
        super.setCompoundDrawables(left, top, right, bottom);
    }

    public void setClearDrawable(Drawable clearDrawable) {
        this.setCompoundDrawables(null, null, clearDrawable, null);
    }

    public boolean isCleanable() {
        return mIsCleanable;
    }
}