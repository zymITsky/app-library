package com.qkun.library.views;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.qkun.library.utils.MyLog;
import com.qkun.library.utils.SpecialPermissionProcessor;
import com.qkun.library.utils.Utils;

public class OverlayWindowView extends View {

    private final WindowManager mWindowManager;

    public OverlayWindowView(Context context) {
        super(Utils.requireNonNull(context).getApplicationContext());
        mWindowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
    }

    public static WindowManager.LayoutParams generateOverlayLayoutParams(int w, int h) {
        int type = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) ?
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY : WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY;

        int flags = WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
                | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
//                | WindowManager.LayoutParams.FLAG_FULLSCREEN
//                | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            flags |= WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                    | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION;
        }

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(w, h, type, flags, PixelFormat.TRANSLUCENT);

        //适配刘海屏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            layoutParams.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        return layoutParams;
    }

    public static WindowManager.LayoutParams generateOverlayLayoutParams() {
        return generateOverlayLayoutParams(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
    }

    @Override
    protected void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public WindowManager getWindowManager() {
        return mWindowManager;
    }

    /**
     * <p>
     * 添加之前需要检查Overlay权限，否则会添加失败
     * </p>
     *
     * @param layoutParams
     */
    public void notifyWindowAddView(ViewGroup.LayoutParams layoutParams) {
        if (Utils.nonNull(layoutParams)) {
            notifyWindowRemoveView();
            if (SpecialPermissionProcessor.canDrawOverlays()) {
                mWindowManager.addView(this, checkLayoutParams(layoutParams));
            } else {
                MyLog.e("Can't draw overlays, maybe you need to request for \"Manifest.permission.SYSTEM_ALERT_WINDOW\" permission.");
            }
        }
    }

    public void notifyWindowAddView() {
        notifyWindowAddView(generateOverlayLayoutParams());
    }

    public void notifyWindowRemoveView() {
        if (Utils.nonNull(getParent())) {
//            mWindowManager.removeViewImmediate(this);
            mWindowManager.removeView(this);
        }
    }

    public void notifyWindowUpdateViewLayout(ViewGroup.LayoutParams layoutParams) {
        if (Utils.nonNull(layoutParams)
                && Utils.nonNull(getParent())
                && SpecialPermissionProcessor.canDrawOverlays()) {
            mWindowManager.updateViewLayout(this, checkLayoutParams(layoutParams));
        }
    }

    private ViewGroup.LayoutParams checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof WindowManager.LayoutParams) {
            return layoutParams;
        }
        return generateOverlayLayoutParams(layoutParams.width, layoutParams.height);
    }

}
