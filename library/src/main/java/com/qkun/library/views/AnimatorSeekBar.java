package com.qkun.library.views;

import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.SeekBar;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatSeekBar;

import com.qkun.library.utils.Utils;

/**
 * @author qinkun
 * @date 8/2 0002 18:17
 * @description
 */
public class AnimatorSeekBar extends AppCompatSeekBar {

    private final Interpolator INTERPOLATOR = new LinearInterpolator();
    private boolean mIsTracking;
    private ValueAnimator mProgressAnimator;
    private final ValueAnimator.AnimatorUpdateListener ANIMATOR_UPDATE_LISTENER = new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            if (Utils.nonNull(animation)) {
                if (mIsTracking) {
                    stop();
                } else {
                    setProgress((int) animation.getAnimatedValue());
                }
            }
        }
    };
    private OnSeekBarChangeListener mSeekBarChangeListener;
    private final OnSeekBarChangeListener SEEK_CHANGE_LISTENER = new OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (Utils.nonNull(mSeekBarChangeListener)) {
                mSeekBarChangeListener.onProgressChanged(seekBar, progress, fromUser);
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            mIsTracking = true;
            if (Utils.nonNull(mSeekBarChangeListener)) {
                mSeekBarChangeListener.onStartTrackingTouch(seekBar);
            }
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            mIsTracking = false;
            if (Utils.nonNull(mSeekBarChangeListener)) {
                mSeekBarChangeListener.onStopTrackingTouch(seekBar);
            }
        }
    };

    public AnimatorSeekBar(Context context) {
        super(context);
    }

    public AnimatorSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AnimatorSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOnSeekBarChangeListener(SEEK_CHANGE_LISTENER);
    }

    @Override
    public void setOnSeekBarChangeListener(OnSeekBarChangeListener l) {
        if (SEEK_CHANGE_LISTENER.equals(l)) {
            super.setOnSeekBarChangeListener(l);
        } else {
            mSeekBarChangeListener = l;
        }
    }

    public void start(int start, int end) {
        start(start, end, Math.abs(end - start));
    }

    public void start(int start, int end, int duration) {
        stop();
        mProgressAnimator = ValueAnimator.ofInt(start, end).setDuration(duration);
        mProgressAnimator.setInterpolator(INTERPOLATOR);
        mProgressAnimator.addUpdateListener(ANIMATOR_UPDATE_LISTENER);
        mProgressAnimator.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void resume() {
        if (Utils.nonNull(mProgressAnimator) && mProgressAnimator.isPaused()) {
            mProgressAnimator.resume();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void pause() {
        if (Utils.nonNull(mProgressAnimator)) {
            mProgressAnimator.pause();
        }
    }

    public void stop() {
        if (Utils.nonNull(mProgressAnimator)) {
            mProgressAnimator.cancel();
            mProgressAnimator = null;
        }
    }

}
